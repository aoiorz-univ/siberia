class CartitemsController < ApplicationController
    
    def create
        cart = Cart.find(session[:cart_id])
        p pro = Product.find(params[:id])
        cartitem = Cartitem.new(qty: params[:qty], product_id: pro.id, cart_id: cart.id )
        cartitem.product = pro
        cart.cartitems << cartitem
        cartitem.save
        
        redirect_to products_path
    end
    
    def destroy
        cow = params[:id]
        Cartitem.destroy_all(product_id: cow, cart_id: session[:cart_id])
        redirect_to products_path
    end
    
end
